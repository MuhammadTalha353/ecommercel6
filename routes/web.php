<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 Route::get('/table', function (){
    return view('table');
 });

 Route::get('/form', function (){
    return view('form');
 });

Route::get('/cv', function (){
    return view('cv');

});

Route::get('/bootstrap', function (){
    return view('bootstrap');

});
Route::get('/grid', function (){
    return view('grid');
});
Route::get('/java', function (){
    return view('java');
});

Route::get('/alert', function (){
    return view('alert');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['as'=>'admin.', 'middleware' => ['auth', 'admin'], 'prefix' => 'admin' ], function (){
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');

    Route::get('categories/{category}/remove', 'CategoriesController@remove')->name('categories.remove');
    Route::get('categories/trash', 'CategoriesController@trash')->name('categories.trash');
    Route::get('categories/recover/{id}', 'CategoriesController@recoverCat')->name('categories.recover');

    Route::get('votes/trash', 'VotesController@trash')->name('votes.trash');
    Route::get('votes/{vote}/remove', 'VotesController@remove')->name('votes.remove');
    Route::get('votes/restore/{id}', 'VotesController@restore')->name('votes.restore');
    Route::get('votes/show', 'VotesController@show')->name('votes.show');

    Route::get('articles/trash', 'ArticleController@trash')->name('articles.trash');
    Route::get('articles/{article}/remove', 'ArticleController@remove')->name('articles.remove');
    Route::get('articles/restore/{id}', 'ArticleController@restore')->name('articles.restore');

    Route::get('tags/article', 'TagsController@show');
    Route::get('tags/show', 'TagsController@articleShow');
    Route::post('tags/search', 'TagsController@search');

    Route::resource('products', 'ProductController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('votes', 'VotesController');
    Route::resource('articles', 'ArticleController');
    Route::resource('tags', 'TagsController');


});

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});
