@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item"> <a href="{{route('admin.dashboard')}}">Dashboard</a> </li>
    <li class="breadcrumb-item"> <a href="{{route('admin.votes.index')}}">Votes</a> </li>
    <li class="breadcrumb-item active" aria-current="page"> Edit Votes</li>
    @endsection
@section('content')
    <form action="{{route('admin.votes.update', $vote->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-row">
            <div class="col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="row d-block">
                <div class="col-sm-12">
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">Name:</label>
                <input type="text" class="form-control" value="{{$vote->name}}" name="name" id="" placeholder="Name" ">
            </div>
            <div class="form-group col-md-6">
                <label for="">CNIC</label>
                <input type="text" class="form-control" value="{{$vote->CNIC}}" name="CNIC" placeholder="CNIC #" >
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity" ">City</label>
                <input type="text" class="form-control" value="{{$vote->city}}" name="city" id="inputCity">
            </div>
            <div class="form-group col-md-6">
                <label for="inputState"  >district</label>
                <select id="inputState" name="district" class="form-control">
                    <option selected>Choose...</option>
                    <option >SARGODHA</option>
                    <option >KARACHI</option>
                    <option >lAHORE</option>
                    <option >PESHAWAR</option>
                    <option >ISLAMABAD</option>

                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Edit Record</button>
    </form>

@endsection