@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.votes.index')}}">Votes</a> </li>
    <li class="breadcrumb-item active" aria-current="page">Trashed Vote</li>

@endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Votes
            </h2>
        </div>
        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-3">
            <a href="/admin/votes/create" class="btn btn-success float-right"> Add Category  </a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>name </th>
                    <th>CNIC</th>
                    <th>city</th>
                    <th>district</th>
                    <th> Action </th>

                </tr>
                </thead>
                <tbody>
                @if($votes)
                    @foreach($votes as $vote)
                        <tr>
                            <td> {{$vote->id}}</td>
                            <td>
                                {{$vote->name}}</td>
                            <td> {{$vote->CNIC}} </td>
                            <td> {{$vote->city}} </td>
                            <td> {{$vote->district}} </td>
                            <td><a class="btn btn-info btn-sm" href="{{route('admin.votes.restore',$vote->id)}}">Restore</a> |
                                <a class="btn btn-danger btn-sm" href="javascript:;" onclick="deleteConfirmed('{{$vote->id}}')">Delete</a>
                                <form id="delete-category-{{$vote->id}}" action="#" method="POST" style="display: none;">
                                @method('DELETE')
                                @csrf
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4"> No Categories Found ... </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>


@endsection
@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-votes-'+id).submit();
            }
        }
    </script>
@endsection