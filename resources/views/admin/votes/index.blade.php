@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item"> <a href="{{route('admin.dashboard')}}"> Dashboard </a> </li>
    <li class="breadcrumb-item active" aria-current="page"> Votes </li>
    @endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Votes Record
            </h2>
        </div>

        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-3">
            <a href="/admin/votes/create" class="btn btn-success float-right"> Add Record  </a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>name </th>
                    <th>CNIC</th>
                    <th>city</th>
                    <th>district</th>
                    <th> Action </th>

                </tr>
                </thead>
                <tbody>
                @if($votes)
                    @foreach($votes as $vote)
                        <tr>
                            <td> {{$vote->id}}</td>
                            <td>
                                {{$vote->name}}</td>
                            <td> {{$vote->CNIC}} </td>
                            <td> {{$vote->city}} </td>
                            <td> {{$vote->district}} </td>
                            <td> <a class="btn btn-success" href="{{route('admin.votes.edit', $vote->id)}}" > Edit</a> |
                                <a class="btn btn-info" href="{{route('admin.votes.show', $vote->id)}}" > View </a>|
                                <a class="btn btn-warning" href="{{route('admin.votes.trash', $vote->id)}}" > trash </a> |
                                <a class="btn btn-danger " href="{{route('admin.votes.remove', $vote->id)}}">Delete</a>

                            </td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-category-'+id).submit();
            }
        }
    </script>
@endsection