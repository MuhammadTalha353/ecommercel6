@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item"> <a href="{{route('admin.dashboard')}}"> Dashboard</a> </li>
    <li class="breadcrumb-item active" aria-current="page">Add Tags</li>
    @endsection
@section('content')
    <form action="{{route('admin.tags.store')}}" method="POST" id="myid">
        @csrf

        <div class="form-group row">
            <div class="col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label class="form-control-label" > <strong> Title: </strong></label>
                <input type="text" class="form-control" id="id1" name="title"  placeholder="Enter title" >

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <label class="control-label " > <strong>Description: </strong></label>
                <textarea name="description" id="editor" class="form-control" rows="10" cols="80"></textarea>
            </div>
        </div>
        <div class="form-group">
            <button type="submit"  class="btn btn-success" id="btn"> Add Tag </button>
        </div>
    </form>
@endsection
@section('script')
    <script type="text/javascript">

        $('#document').ready(function() {
            var url = $(this).attr("action");

            $('#btn').click(function () {
                $.post(url,
                    {
                       Title:  $('#id1').val(),
                       Description: $('#editor').val(),
                    },
                    function(data, status) {
                        alert("Data: " + data + "\nStatus: " + status);
                    });
            });
    });

    </script>

@endsection