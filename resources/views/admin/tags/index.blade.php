@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"> Dashboard </a></li>
    <li class="breadcrumb-item active" aria-current="page" > Tags </li>
    @endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2> Tags </h2>
        </div>

    <div class="row d-block">
        <div class="col-sm-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>

    <div class="col-3">
        <a href="/admin/tags/create" class="btn btn-success float-right"> Add Tag  </a>
    </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
            <th> # </th>
            <th> title  </th>
            <th> description</th>
                <th> action </th>
            </tr>
            </thead>

            <tbody>
            @if($tags)
                @foreach($tags as $tag)
            <tr>
                <td> {{$tag->id }} </td>
                <td> {{$tag->title }} </td>
                <td> {!! $tag->description  !!} </td>
                <td>
                <a class="btn btn-success" href="{{route('admin.tags.edit', $tag->id)}}" > Edit</a> |
                    <a class="btn btn-danger btn-sm" href="javascript:;" onclick="deleteConfirmed('{{$tag->id}}')">Delete</a>
                    <form id="delete-category-{{$tag->id}}" action="{{ route('admin.tags.destroy', $tag->id) }}" method="POST" style="display: none;">
                    @method('DELETE')
                    @csrf
                </td>
            </tr>
            @endforeach
                @endif
            </tbody>

        </table>

    </div>

    @endsection

@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-category-'+id).submit();
            }
        }
    </script>
@endsection