@extends('admin.app')
@section('breadcrumbs')


    @endsection
@section('content')
    <form class="form-group inline" action="/admin/tags/search" method="POST">
        @csrf
        <div class="col-sm-12">
        <div class="col-sm-9">
            <select  name="tag_id[]" id="tag_id" class="form-control" value="" multiple >
                <option value="0"> Top Level</option>
                @foreach($tags as $tag)
                    <option value="{{$tag->id}}">
                        {{$tag->title}}   </option>
                    @endforeach
            </select>
                <div class="col-sm-3">
        <button type="submit" class="btn btn-primary mb-2"> Submit </button>
                </div>
            </div>
        </div>
    </div>
    </form>
    @endsection
@section('scripts')
    <script type="text/javascript">
        $('#document').ready(function () {
            $('#tag_id').select2({
                placeholder: "select a tag",
            });
        });


    </script>
@endsection

