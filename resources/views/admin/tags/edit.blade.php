@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item"> <a href="{{route('admin.dashboard')}}"> Dashboard</a> </li>
    <li class="breadcrumb-item active" aria-current="page">Edit Tags</li>
@endsection
@section('content')
    <form action="{{route('admin.tags.update', $tag->id)}}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group row">
            <div class="col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label class="form-control-label" > <strong> Title: </strong></label>
                <input type="text" class="form-control " value="{{$tag->title}}" name="title"  placeholder="Enter title" >

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <label class="control-label " > <strong>Description: </strong></label>
                <textarea name="description" id="editor" class="form-control" rows="10" cols="80"> {!! $tag->description !!} </textarea>
            </div>
        </div>
        <div class="form-group">
            <button type="submit"  class="btn btn-success"> Edit Tag </button>
        </div>


@endsection