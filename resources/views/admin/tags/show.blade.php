@extends('admin.app')
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Articles
            </h2>
        </div>
        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>title </th>
                    <th>Description</th>
                    <th> Published at</th>

                </tr>
                </thead>
               <tbody>
                @if($articles)
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$article->id}}</td>
                            <td> {{$article->title}} </td>
                            <td> {{$article->description}}</td>
                            <td> {{$article->published_at}}</td>
                        </tr>
                        @endforeach
                @endif
                </tbody>
            </table>
        </div>
        @endsection


