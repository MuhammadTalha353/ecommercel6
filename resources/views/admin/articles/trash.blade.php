@extends('admin.app')
@section('breadcrumbs')
    <li class="breadcrumb"> <a href="{{route('admin.dashboard')}}"> Dashboard </a> </li>
    <li class="breadcrumb active" aria-current="page"> article </li>
@endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Articles
            </h2>
        </div>
        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-3">
            <a href="{{route('admin.articles.create')}}" class="btn btn-success float-right"> Add Article  </a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>title </th>
                    <th>Description</th>
                    <th> Published at</th>
                    <th> Action </th>
                </tr>
                </thead>
                <tbody>
                @if($articles)
                    @foreach($articles as $article)
                        <tr>
                            <td>{{$article->id}}</td>
                            <td> {{$article->title}} </td>
                            <td> {{$article->description}}</td>
                            <td> {{$article->published_at}}</td>
                            <td> <a class="btn btn-primary" href="{{route('admin.articles.restore', $article->id)}}" >Restore </a> |
                                <a class="btn btn-danger btn-sm" href="javascript:;" onclick="deleteConfirmed('{{$article->id}}')">Delete</a>
                                <form id="delete-category-{{$article->id}}" action="{{route('admin.articles.destroy', $article->id)}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-category-'+id).submit();
            }
        }
    </script>
@endsection