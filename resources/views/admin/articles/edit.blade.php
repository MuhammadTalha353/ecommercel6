@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.articles.index')}}">Articles </a></li>
    <li class="breadcrumb-item active" aria-current="page"> Edit Article</li>
@endsection
@section('content')

    <form action="{{route('admin.articles.update', $article->id)}}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
            <div class="col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label class="form-control-label" > <strong> Title: </strong></label>
                <input type="text" class="form-control " name="title" value="{{$article->title}}" placeholder="Enter title" >
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="control-label " > <strong>Description: </strong></label>
                <textarea name="description" id="editor" class="form-control" rows="10" cols="80"> {!! $article->description !!} </textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="control-label " > <strong>Published: </strong></label>
                    <input type="date" class="form-control" name="published_at" value="{{$article->getFromDateAttribute($article->published_at)}}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label class="form-control-label"> <strong>  Select Tag:</strong> </label>
                <select  name="tag_id[]" id="tag_id" class="form-control" value="" multiple >
                    <option value="0"> Top Level</option>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">
                            {{$tag->title}}   </option>
                    @endforeach
                </select>
            </div>
        </div>



                <div class="form-group row">
                    <button type="submit"  class="btn btn-primary"> Edit Article </button>
                </div>

        @endsection
    </form>
@section('scripts')
    <script type="text/javascript">

        ClassicEditor.create(document.querySelector('#editor'), {
            toolbar: ['Heading', 'Link', 'bold', 'italic', 'bulletedList', 'undo', 'redo'],
        }).then(editor => {
            console.log(editor);
        })
            .catch(error => {
                console.error(error);
            });
        $('#document').ready(function () {
            $('#tag_id').select2({
                placeholder: "select a tag",
            });
        });

    </script>
@endsection

