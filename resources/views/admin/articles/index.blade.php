@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item active" aria-current="page"> Articles </li>
    @endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Articles
            </h2>
        </div>
        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-3">
            <a href="{{route('admin.articles.create')}}" class="btn btn-success float-right"> Add Article  </a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>title </th>
                    <th>Description</th>
                    <th> Published at</th>
                    <th> Tags </th>
                    <th> Action </th>
                </tr>
                </thead>
    <tbody>
    @if($articles)
        @foreach($articles as $article)
        <tr>
            <td>{{$article->id}}</td>
            <td> {{$article->title}} </td>
            <td> {{$article->description}}</td>
            <td> {{$article->published_at}}</td>
            <td> @if($article->tag()->count() > 0)
                  @foreach($article->tag as $tag)
                      {{$tag->title}}
                      @endforeach

            @endif
            </td>
            <td> <a class="btn btn-primary" href="{{route('admin.articles.edit', $article->id)}}" >Edit </a> |
                <a class="btn btn-info" href="{{route('admin.articles.trash', $article->id)}}"> Trash</a> |
                <a class="btn btn-success" href="{{route('admin.articles.show', $article->id)}}"> view </a>
                <a class="btn btn-danger" href="{{route('admin.articles.remove', $article->id)}}" > Delete </a> |
                 </td>
            <form id="delete-category-{{$article->id}}" action="{{ route('admin.articles.destroy', $article->id) }}" method="POST" style="display: none;">
            @method('DELETE')
            @csrf
        </tr>
        @endforeach
        @endif
    </tbody>
            </table>
        </div>
    </div>
    @endsection

@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-category-'+id).submit();
            }
        }
    </script>
@endsection