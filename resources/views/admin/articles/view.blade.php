@extends('admin.app')
@section('breadcrumbs')
    <li class="breadcrumb"> <a href="{{route('admin.dashboard')}}"> Dashboard </a> </li>
    <li class="breadcrumb active" aria-current="page"> article </li>
    @endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
              View  Articles
            </h2>
        </div>

        <div class="col-3">
            <a href="{{route('admin.articles.create')}}" class="btn btn-success float-right"> Add Article  </a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>title </th>
                    <th>Description</th>
                    <th> Published at</th>
                    <th> Action </th>
                </tr>
                </thead>
    <tbody>

        <tr>
            <td>{{$article->id}}</td>
            <td> {{$article->title}} </td>
            <td> {{$article->description}}</td>
            <td> {{$article->published_at}}</td>
            <td> <a class="btn btn-danger" href="{{route('admin.articles.remove', $article->id)}}" > Delete </a>
                 </td>

        </tr>

    </tbody>
            </table>
        </div>
    </div>
    @endsection

