@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.articles.index')}}">Articles </a></li>
    <li class="breadcrumb-item active" aria-current="page"> Add Article</li>
@endsection
@section('content')

    <form action="{{route('admin.articles.store')}}" method="POST" id="myform">
        @csrf

        <div class="form-group row">
            <div class="col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <label class="form-control-label" > <strong> Title: </strong></label>
                <input type="text" class="form-control " name="title"  placeholder="Enter title" >

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="control-label " > <strong>Description: </strong></label>
                <textarea name="description" id="editor" class="form-control" rows="10" cols="80"></textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="control-label " > <strong>Published: </strong></label>
                <input type="date" class="form-control" name="published_at" value="date(m-d-y)">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="form-control-label"> <strong>  Select Tag:</strong> </label>
            <select  name="tag_id[]" id="tag_id" class="form-control" multiple >
                    <option value="0"> Top Level</option>
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}">
                            {{$tag->title}}   </option>
                    @endforeach

            </select>
        </div>
        </div>
        <div class="form-group row">
                    <button type="submit" id="btn" class="btn btn-primary"> Add Article </button>
            </div>


        @endsection
    </form>
@section('scripts')
    <script type="text/javascript">

        ClassicEditor.create(document.querySelector('#editor'), {
            toolbar: ['Heading', 'Link', 'bold', 'italic', 'bulletedList', 'undo', 'redo'],
        }).then(editor => {
            console.log(editor);
        })
            .catch(error => {
                console.error(error);
            });


        $('#document').ready(function() {
            $(#myform).submit(function(event) {
                event.preventDefault();
                var post_url = $(this).attr("action");
                var method = $(this).attr("method");
                var form = $(this).serialize();
            });

                $.ajax({
                   url: post_url,
                   type: method,
                    data: form,
                }).done(function(response) {
                    $("#server-result").html(response);
                });

            });
            $('#tag_id').select2({
                placeholder: "select a tag",
            });
        });

    </script>
@endsection

