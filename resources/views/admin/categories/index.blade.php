@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item active" aria-current="page">categories</li>

@endsection
@section('content')
<div class="row">
    <div class="col-6">
        <h2>
            Categories
        </h2>
    </div>
    <div class="row d-block">
        <div class="col-sm-12">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
        </div>
    </div>
    <div class="col-3">
        <a href="/admin/categories/create" class="btn btn-success float-right"> Add Category  </a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>title </th>
                <th>description</th>
                <th>categories</th>
                <th> Date Created</th>
                <th> Action </th>
            </tr>
            </thead>
            <tbody>
            @if($categories)
                @foreach($categories as $category)
            <tr>
                <td> {{$category->id}}</td>
                <td>{{$category->title}}</td>
                <td>{!! $category->description !!}</td>
                <td>
                    @if($category->children()->count()> 0)
                        @foreach($category->children as $children)
                            {{$children->title}}
                            @endforeach
                    @else
                        {{"Parent Category"}}
                        @endif
                </td>
                <td>{{$category->created_at}}</td>
                <td><a class="btn btn-info btn-sm" href="{{route('admin.categories.edit',$category->id)}}">Edit</a> |
                    <a class="btn btn-success btn-sm" href="{{route('admin.categories.trash',$category->id)}}"> Trash </a> |
                    <a class="btn btn-danger btn-sm" href="javascript:;" onclick="deleteConfirmed('{{$category->id}}')">Delete</a>
                    <form id="delete-category-{{$category->id}}" action="{{ route('admin.categories.destroy', $category->id) }}" method="POST" style="display: none;">
                    @method('DELETE')
                    @csrf
                </td>
            </tr>
            @endforeach
            @else
                <tr>
                    <td colspan="4"> No Categories Found ... </td>
                </tr>
                @endif
            </tbody>
        </table>
            </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{$categories->links()}}
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function deleteConfirmed(id) {
            let choice = confirm('are you sure, you want to delete this message');
            if (choice){
                document.getElementById('delete-category-'+id).submit();
            }
        }
    </script>
    @endsection