@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " ><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.categories.index')}}">categories </a></li>
    <li class="breadcrumb-item active" aria-current="page"> Add/Edit Category</li>
@endsection
@section('content')

    <form action="@if(isset($category)) {{route('admin.categories.update', $category->id)}}
        @else {{route('admin.categories.store')}} @endif" method="POST" id="formid">
        @csrf
        @if(isset($category))
            @method('PUT')
            @endif
        <div class="form-group row">
        <div class="col-sm-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        </div>

            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group" >
            <div class="col-sm-12">
            <label class="form-control-label" > <strong> Title: </strong></label>
                <input type="text" class="form-control " name="title"  placeholder="Enter title" value="{{@$category->title}}">
                <p class="small"> {{config('app,url')}} <span id="url"></span> </p>
                <input type="hidden" name="slug" id="slug" value="">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
            <label class="form-control-label"> <strong>  Description: </strong></label>

                        <textarea name="description" id="editor" class="form-control" rows="10" cols="80">{!! @$category->description !!}</textarea>
            </div>
            </div>

            <div class="col-sm-12">
                <label class="form-control-label"> <strong>  Select Category:</strong> </label>
                <select  name="parent_id[]" id="parent_id" class="form-control" multiple >
                    @if(isset($categories))
                        <option value="0"> Top Level</option>
                        @foreach($categories[0]->children as $category)
                            <option value="{{$category['id']}}">
                            {{$category['title']}}   </option>
                        @endforeach
                        @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <div class="checkbox">
                    <label><input type="checkbox" > <b> Remember me </b></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                @if(isset($category))
                    <button type="submit"  class="btn btn-primary"> Edit Category </button>
                @else
                    <button type="submit"  class="btn btn-primary"> Add Category </button>
                @endif
            </div>
        </div>

        @endsection
    </form>
    @section('scripts')
        <script type="text/javascript">

                ClassicEditor.create(document.querySelector('#editor'), {
                    toolbar: ['Heading', 'Link', 'bold', 'italic', 'bulletedList', 'undo', 'redo'],
                }).then(editor => {
                    console.log(editor);
                })
                    .catch(error => {
                        console.error(error);
                    });
                $('#txturl').on('keyup', function () {
                    var url = slugify($(this).val());
                    $('#url').html(url);
                    $('#slug').var(url);
                });
                $('#document').ready(function () {

                });
                    $('#parent_id').select2({
                        placeholder: "select a parent category",
                    });
                });


                $('.datepicker').datepicker();

        </script>
    @endsection
