@extends('admin.app')
@section('breadcrumb')
    <li class="breadcrumb-item " xmlns="http://www.w3.org/1999/html"><a href="{{route('admin.dashboard')}}">Dashboard </a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.products.index')}}">Products</a></li>
    <li class="breadcrumb-item active" aria-current="page"> Add/Edit Category</li>
@endsection
@section('content')
    <h1 class="model-title"> Add / Edit Product </h1>
    <div class="container">
        <div class="col-m-12" id="main">
    <form id="myid" action="/admin/products/store" method="POST">
        <div class="row">
        @csrf
        <div class="col-lg-8">
        <div class="form-group row">
            <div class="col-lg-12">
            <label class="form-control-label form-control-lg" > <strong> Title: </strong></label>
                <input type="text" class="form-control " name="title"  placeholder="Enter title" value="{{@$product->title}}">
                <p class="small"> {{config('app,url')}} <span id="url"></span> </p>
                <input type="hidden" name="slug" id="slug" value="">
            </div>
        </div>

        <div class="form-group row">
            <label class="control-label form-control-lg" > <strong>Description: </strong></label>
            <div class="col-lg-12">
                <textarea name="description" id="editor" class="form-control" rows="10" cols="80">{!! @$product->description !!}</textarea>
            </div>
            </div>

            <div class="form-row align-items-center">
            <div class="col-6">
                <label class="form-control-label form-control-lg" > <strong> Price: </strong></label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"> $</span>
                    </div>
                    <input type="text" class="form-control" aria-label="username" placeholder="0.00">
                </div>
            </div>
                <div class="col-6">
                    <label class="form-control-label form-control-lg" > <strong> Discount: </strong></label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"> $</span>
                        </div>
                        <input type="text" class="form-control" name="discount_price" aria-label="discount_price"
                               aria-describedby="discount"  placeholder="0.00" value="{{@$product->discount_price}}">
                    </div>
                </div>
            </div>
        <div class="form-group row">
            <div class="card col-sm-12 p-4 mb-2 ">
            <div class="card-header align-items-center"> Extra Option: </div>
                <div class="float-right">
                <button type="button" id="btn-add" class="btn btn-primary btn-sm" > > </button>
                <button type="button" id="btn-remove" class="btn btn-danger btn-sm" > < </button>
                </div>
            <div class="card-body" id="extras">
            <div class="row align-items-center options">
                    <div class="col-sm-4">
                        <label class="form-control-label "> option:  <span class="count"> 1 </span></label>
                        <input type="text" class="form-control" name="extra[option][]" placeholder="size" value="">
                    </div>
                    <div class="col-sm-8" >
                        <label class="form-control-label "> values:</label>
                        <input type="text" class="form-control" name="extra[value][]" placeholder="Price1| Price2| Price3" value="">
                        <label class="form-control-label "> Additional Price:</label>
                        <input type="text" class="form-control" name="extra[price][]" placeholder="Price1| Price2| Price3" value="">
                    </div>
                </div>
            </div>
                <div class="row align-items-center options">
                    <div class="col-sm-4">
                        <label class="form-control-label "> option:  <span class="count"> 2 </span></label>
                        <input type="text" class="form-control" name="extra[option][]" placeholder="size" value="">

                    </div>
                    <div class="col-sm-8" >
                        <label class="form-control-label "> values:</label>
                        <input type="text" class="form-control" name="extra[values][]" placeholder="Price1| Price2| Price3" value="">
                        <label class="form-control-label "> Additional Price:</label>
                        <input type="text" class="form-control" name="extra[price][]" placeholder="Price1| Price2| Price3" value="">
                    </div>
                </div>
                <div class="row align-items-center options">
                    <div class="col-sm-4">
                        <label class="form-control-label "> option:  <span class="count"> 3 </span></label>
                        <input type="text" class="form-control" name="extra[option][]" placeholder="size" value="">

                    </div>
                    <div class="col-sm-8" >
                        <label class="form-control-label "> values:</label>
                        <input type="text" class="form-control" name="extra[values][]" placeholder="Price1| Price2| Price3" value="">
                        <label class="form-control-label "> Additional Price:</label>
                        <input type="text" class="form-control" name="extra[price][]" placeholder="Price1| Price2| Price3" value="">
                    </div>
                </div>
            </div>
            </div>
        </div>


            <div class="col-lg-4">
                <ul class="list-group-row">
                    <li class="list-group-item active"> <h5> status  </h5></li>
                    <li class="list-group-item">
                        <div class="form-group-row">
                        <select class="form-control" id="status">
                        <option value="1"> pending</option>
                            <option value="2"> publish</option>
                        </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Add Product">
                        </div>
                        </div>
                    </li>
                    <li class="list-group-item active"><h5>Featured Image</h5></li>
                    <li class="list-group-item">
                        <div class="input-group mb-3">
                            <div class="custom-file ">
                                <input type="file"  class="custom-file-input" name="thumbnail" id="thumbnail">
                                <label class="custom-file-label" for="thumbnail">Choose file</label>
                            </div>
                        </div>
                        <div class="img-thumbnail  text-center">
                            <img src="@if(isset($product)) {{asset('images/'.$product->thumbnail)}}
                            @else {{asset('images/no-thumbnail.jpeg')}} @endif" id="imgthumbnail" class="img-fluid" alt="">
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="col-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="featured"><input type="checkbox" name="discount" value="0" /></span>
                                </div>
                                <span type="text" class="form-control" name="featured" placeholder="0.00" aria-label="featured"
                                   aria-describedby="featured" >Featured Product</span>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item active"> <h5>Select Categories </h5> </li>
                    <li class="list-group-item">
                        <select name="category_id" id="select2" class="form-control" multiple>
                            @if($categories->count() > 0 )
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}"> {{$category -> title}}</option>
                                @endforeach
                            @endif
                        </select>
                    </li>
                </ul>
            </div>
            </div>
        </form>
        </div>
    </div>

        @endsection
        @section('scripts')
            <script type="text/javascript">

                ClassicEditor.create(document.querySelector('#editor'), {
                    toolbar: ['Heading', 'Link', 'bold', 'italic', 'bulletedList', 'undo', 'redo'],
                }).then(editor => {
                    console.log(editor);
                })
                    .catch(error => {
                        console.error(error);
                    });
                $('#txturl').on('keyup', function () {
                    const url = slugify($(this).val());
                    $('#url').slugify(url);
                    $('#slug').var(url);
                });
                $('#select2').select2({
                    placeholder: "select a multiple categories",
                    allowclear: true,
                });
                $('#status').select2({
                    placeholder: "select a multiple categories",
                    allowclear: true,
                    minimumResultsForSearch: Infinity
                });
                $('#thumbnail').on('change', function ( ) {
                    var file = $(this).get(0).files;
                    var reader = new FileReader();
                    reader.readAsDataURL(file[0]);
                    reader.addEventListener("load", function (e) {
                        var image = e.target.result;
                 $("#imgthumbnail").attr('src', image);

                    });
                });
                $('#btn-add').on('click', function (e) {
                    var count = $('.options').length+1;
                    $('#extras').append(' <div class="row align-items-center options">\n' +
                        '                    <div class="col-sm-4">\n' +
                        '                        <label class="form-control-label "> option:  <span class="count"> 1 </span></label>\n' +
                        '                        <input type="text" class="form-control" name="extra[option][]" placeholder="size" value="">\n' +
                        '\n' +
                        '                    </div>\n' +
                        '                    <div class="col-sm-8" >\n' +
                        '                        <label class="form-control-label "> values:</label>\n' +
                        '                        <input type="text" class="form-control" name="extra[values][]" placeholder="Price1| Price2| Price3" value="">\n' +
                        '                        <label class="form-control-label "> Additional Price:</label>\n' +
                        '                        <input type="text" class="form-control" name="extra[price][]" placeholder="Price1| Price2| Price3" value="">\n' +
                        '                    </div>\n' +
                        '                </div>')

                });
                $('#btn-remove').on('click', function(e){
                    if($('.options').length > 1){
                        $('.options:last').remove();
                    }
                });
                $(document).ready(function() {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

                @endsection
