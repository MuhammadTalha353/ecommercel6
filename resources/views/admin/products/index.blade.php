@extends('admin.app')
@section('breadcrumb')
        <li class="breadcrumb-item active" aria-current="page"> Product </li>
@endsection
@section('content')
    <div class="row">
        <div class="col-6">
            <h2>
                Products
            </h2>
        </div>
        <div class="row d-block">
            <div class="col-sm-12">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>
        <div class="col-3">
            <a href="/admin/products/create" class="btn btn-success float-right"> Add Product  </a>
        </div>

@endsection