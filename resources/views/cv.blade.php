<!DOCTYPE html>
<html lang="en">
<head>
    <title>CV assignment </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Style the header */
        header {
            background-color: #666;
            padding: 5px;
            text-align: center;
            font-size: 30px;
            color: white;
        }

        /* Container for flexboxes */
        section {
            display: -webkit-flex;
            display: flex;
        }

        /* Style the navigation menu */
        nav {
            -webkit-flex: 1;
            -ms-flex: 1;
            flex: 1;
            background: #ccc;
            padding: 20px;
        }

        /* Style the list inside the menu */
        nav ul {
            list-style-type: square;
            padding: 10px;
        }

        /* Style the content */
        article {
            -webkit-flex: 3;
            -ms-flex: 3;
            flex: 3;
            background-color: #f1f1f1;
            padding: 10px;
            margin-left: 10px;
            list-style-type: disc;
        }


        /* Style the footer */
        footer {
            background-color: #777;
            padding: 10px;
            text-align: center;
            color: white;
        }

        .table,th,td {
            border: 2px solid black;
            border-collapse: collapse;
            text-align: center;

        }

        /* Responsive layout - makes the menu and the content (inside the section) sit on top of each other instead of next to each other */
        @media (max-width: 600px) {
            section {
                -webkit-flex-direction: column;
                flex-direction: column;
            }
        }
    </style>
</head>
<body>
<header>
    <h2>IQRA</h2>

 </header>

<section>
    <nav>
        <img src="C:\wamp64\www\ecommercel6\1555521042-167398l031519x-1555521177.jpg" alt="Italian Trulli" width="100px" height="100px">

        <u> <b>Hobbies </b></u>
        <ul>
            <li> Reading </li>
            <li> Planting</li>
        </ul>
            <u> <b> Skills</b>  </u>
            <ul>
            <li> html </li>
                <li> CSS </li>
                <li>
                    Java Script
                </li>
                <li> word press </li>
            </ul>
        <u> <b> Languages</b>  </u>

        <ul>
        <li> Urdu</li>
            <li> English </li>
            <li> Punjabi</li>
        </ul>
    </nav>

    <article>
        <h2> Profile: </h2>
        <ul>
            <li>  <b>Father Name: </b> &nbsp; &nbsp; &nbsp;  Muhammad Latif  </li>
            <li>  <b>CNIC: </b> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; 38403-00000-0 </li>
            <li>  <b>Nationality: </b>  &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; Pakistan  </li>
            <li>  <b>Religion: </b>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; Islam  </li>
            <li>  <b>Contact #: </b>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 0341-5674321  </li>
            <li>  <b>Address: </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; Bismillah Park  </li>
            <li>  <b>Email: </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; areehagul@gmail.com  </li>
        </ul>
        <hr>
        <h2> Educational Record</h2>
        <table class="table" style="width: 70%">
        <tr>
            <th > Degree Program </th>
            <th> Passing Year </th>
            <th> Grade / Division </th>
        </tr>
            <tr>
                <td> Masters</td>
                <td> 2017</td>
                <td> First</td>
            </tr>
            <tr>
                <td> B.Sc (COmputer Science)</td>
                <td> 2015</td>
                <td> First</td>
            </tr>
        </table>
        <br>
        <hr>
        <h2> Experience </h2>
        <ul>
            <li> 5 years in CGSS </li>
            <li> 1 year in MGS</li>
        </ul>

    </article>

</section>

<footer>
    <p><b> Referrence: </b>  will be furnished on demand </p>
</footer>

</body>
</html>
