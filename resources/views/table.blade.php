<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        </style>
</head>
<body>
<h1> Col Span </h1>
<table>
    <tr>
        <th> Name</th>
        <th> roll no</th>
        <th colspan="2"> address </th>
        <th> phone #</th>
    </tr>
    <tr>
        <td> iqra </td>
        <td> 1 </td>
        <td> house # 39</td>
        <td> bismillah park</td>
        <td>0000000000 </td>
    </tr>

</table>

<h2> Row Span </h2>
<table>
    <tr>
        <th> name </th>
        <td> iqra </td>
    </tr>
    <tr>
        <th> program</th>
        <td> masters</td>
    </tr>
    <tr>
    <th rowspan="3"> subjects </th>
        <td> C++</td>
    </tr>
    <tr>
        <td>Java </td>
    </tr>
    <tr>
        <td> Laravel </td>

    </tr>
</table>
</body>
</html>