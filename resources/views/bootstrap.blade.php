<html>
<head>
    <title>Bootstrap Assignment</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
        body {
            font: 400 15px/1.8 Lato, sans-serif;
            color: #777;
        }
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .box{

            margin: 80px 120px;


        }
        h3, h4 {
            letter-spacing: 10px;
            padding: 20px;
        }
        .margin{
            margin-left: 120px;
            margin-right: 120px;
        }
        .bg{
            background-color: black;
            color: white;
        }

        .img-responsive {
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item">
            <a class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Band</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Tour</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="#">Contact</a>
        </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        More
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Marchandise</a>
                        <a class="dropdown-item" href="#">Extra</a>
                        <a class="dropdown-item" href="#">More</a>
                    </div>
                </li>
    </ul>
    </div>
</nav>
</div>
<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{asset('images/la.jpg')}}"  width="1100" height="500">
            <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{asset('images/Chicago.jpg')}}" alt="Chicago" width="1100" height="500">
            <div class="carousel-caption">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{asset('images/ny.jpg')}}" alt="New York" width="1100" height="500">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
<br>
<div id="band" class="container text-center">
<h3> THE BAND</h3>
    <p><em>We love music!</em></p>
    <p class="text-justify text-center"> We have created a fictional band website. Lorem ipsum dolor sit amet,
        consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
        in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
        ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

<br>
<div class="row margin">
    <div class="col">
        <p class="text-center"> <strong> Name</strong> </p> <br>
        <a href="#demo" data-toggle="collapse">
        <img src="{{asset('images/bandmember.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="255" height="255">
        </a>
        <div id="demo" class="collapse">
            <p>Guitarist and Lead Vocalist</p>
            <p>Loves long walks on the beach</p>
            <p>Member since 1988</p>
        </div>
    </div>
    <div class="col">
        <p class="text-center"> <strong> Name</strong> </p><br>
        <a href="#demo2" data-toggle="collapse">
        <img src="{{asset('images/bandmember.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="255" height="255">
        </a>
        <div id="demo2" class="collapse">
            <p>Drummer</p>
            <p>Loves drummin'</p>
            <p>Member since 1988</p>
        </div>
    </div>
    <div class="col">
        <p class="text-center"> <strong> Name</strong> </p><br>
        <a href="#demo3" data-toggle="collapse">
            <img src="{{asset('images/bandmember.jpg')}}" class="rounded-circle" alt="Cinque Terre" width="255" height="255">
        </a>
        <div id="demo3" class="collapse">
            <p>Bass player</p>
            <p>Loves math</p>
            <p>Member since 2005</p>
        </div>
    </div>
    <br>
</div>
    </div>

<div id="tour" class="bg">
    <div class="box">
        <h3 class="text-center">  TOUR DATES </h3>
        <p class="text-center"> <em> Lorem ipsum we'll play you some music. <br> <em> Remember to book your tickets! </em> </p> <br>
       <div>
        <ul class="list-group">
            <li class="list-group-item list-group-item-action">september <span class="badge badge-danger">Sold Out!</span></li>
            <li class="list-group-item list-group-item-action">October <span class="badge badge-danger"> Sold Out!</span> </li>
            <li class="list-group-item list-group-item-action">November <span class="badge badge-primary"> 3</span> </li>
        </ul>
       </div>
        <div class="row  text-center">
            <div class="col-sm-4">
                    <div class="img-thumbnail">
                    <img src="{{asset('images/paris.jpg')}}" width="340" height="300"> <br>
                    <p class="text-center" style="color: black;"><strong>Paris</strong></p>
                    <p style="color: black;">Friday 27 November 2015</p>
                    <button class="btn btn-dark" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
                </div>
            <div class="col-sm-4">
                <div class="img-thumbnail">
                    <img src="{{asset('images/newyork.jpg')}}" width="340" height="300"> <br>
                    <p class="text-center" style="color: black;"><strong>New York</strong></p>
                    <p style="color: black;">Saturday 28 November 2015</p>
                    <button class="btn btn-dark" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="img-thumbnail">
                    <img src="{{asset('images/sanfran.jpg')}}" width="340" height="300"> <br>
                    <p class="text-center" style="color: black;"><strong>San Francisco</strong></p>
                    <p style="color: black;">Sunday 29 November 2015</p>
                    <button class="btn btn-dark" data-toggle="modal" data-target="#myModal">Buy Tickets</button>
                </div>
            </div>
    </div>
    </div>
</div>

<div class="box">
    <h4 class="text-center">  Contact </h4>
    <p class="text-center"> <em> We love our fans!</em></p> <br>

<div class="row">
        <div class="col-md-4">
            <p>Fan? Drop a note.</p>
            <p><span class="glyphicon glyphicon-map-marker"></span>Chicago, US</p>
            <p><span class="glyphicon glyphicon-phone"></span>Phone: +00 1515151515</p>
            <p><span class="glyphicon glyphicon-envelope"></span>Email: mail@mail.com</p>
        </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-sm-6 form-group">
            <input type="text" name="name" placeholder="Name" class="form-control" required>
            </div>
            <div class="col-sm-6 form-group">
            <input type="email" name="email" placeholder="Email" class="form-control" required>
            </div>
        </div>
            <textarea class="form-control" name="textarea" placeholder="Enter Description" rows="5"></textarea>
<br>
            <div class="row">
                <div class="col-md-12 form-group">
                    <button class="btn btn-dark pull-right" type="submit"> send</button>
            </div>
            </div>
    </div>
    </div>

</div>
        <div class="box text-center">
            <h3 class="text-center">From The Blog</h3>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Mike</a></li>
                <li><a data-toggle="tab" href="#menu1">Chandler</a></li>
                <li><a data-toggle="tab" href="#menu2">Peter</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <h2>Mike Ross, Manager</h2>
                    <p>Man, we've been on the road for some time now. Looking forward to lorem ipsum.</p>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <h2>Chandler Bing, Guitarist</h2>
                    <p>Always a pleasure people! Hope you enjoyed it as much as I did. Could I BE.. any more pleased?</p>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <h2>Peter Griffin, Bass player</h2>
                    <p>I mean, sometimes I enjoy the show, but other times I enjoy other things.</p>
                </div>
            </div>
        </div>
            <img src="{{asset('images/map.jpg')}}" class="img-responsive" style="width:100%">

<footer class="text-center">
    <a class="up-arrow" href="#myPage"  title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Bootstrap Theme Made By <a href="https://www.w3schools.com" data-toggle="tooltip" title="Visit w3schools">www.w3schools.com</a></p>
</footer>
</body>
</html>