/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */


require('./bootstrap');
import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';



require('./bootstrap');
const feather = require('feather-icons');
window.ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
window.select2 = require('select2');
feather.replace();
window.slugify = function (text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')

}



