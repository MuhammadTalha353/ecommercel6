<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Profile;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'customer',
            'description' => 'customer role'
        ]);

        $role = Role::create([
            'name' => 'Admin',
            'description' => 'admin role'
        ]);

        $user = User::create([
            'role_id' => $role->id,
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret')

        ]);

        Profile::create([
            'user_id' => $user->id
        ]);
    }
}
