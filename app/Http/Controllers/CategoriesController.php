<?php

namespace App\Http\Controllers;

use App\Category;
use foo\bar;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\TestFixture\C;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(3);
        return view('admin.categories.index', compact('categories'));
    }
    public function trash()
    {
        $categories = Category::onlyTrashed()->paginate(3);
        return view('admin.categories.trash', compact('categories'));
    }
    public function create()
    {
       $categories = Category::all();
        return view('admin.categories.create')->with(['catagories'=> $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required | min:5',
            'description' => 'required'
        ]);
         $categories = Category::create([
           'title' => request('title'),
           'description' => request('description')
        ]);
         $categories->children()->attach($request->parent_id);
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category $category
     * @return void
     */
    public function update(Request $request, Category $category)
    {
        $category->title = $request->title;
        $category->description = $request->description;
        $category->save();
        $category->children()->detach();
        $category->children()->attach($request->parent_id);
        return  back()->with('message', 'record successfully updated');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return void
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::with('Children')->where('id' , $category->id)->get();
        //return print_r($categories[0]->children);
        return view('admin.categories.create', ['categories' =>$categories, 'category' => $category]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $categories
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function recoverCat($id)
    {
        $category = Category::withTrashed()->findOrFail($id);
        if($category->restore()) {
            return back()->with('message', 'Restore Successfully');

        } else {
            return back()->with('messege','Restore Error');
        }
    }
    public function destroy(Category $category)
{
    if($category->children()->detach() && $category->forceDelete()) {
        return back()->with('message', 'Deleted Successfully');

    } else {
        return back()->with('messege','Delete Error');
    }
}

    public function remove(Category $category)
    {
        if($category->Delete()) {
            return back()->with('message', 'Trashed Successfully');

        } else {
            return back()->with('messege','Trashed Error');
        }
    }
}
