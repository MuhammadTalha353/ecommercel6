<?php

namespace App\Http\Controllers;

use App\Vote;
use Illuminate\Http\Request;

class VotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $votes = Vote::all();
        return view('admin.votes.index', compact('votes'));
    }
    public function view()
    {
        return view('admin.votes.view');
    }
    public function trash()
    {
        $votes = Vote::onlyTrashed()->paginate(10);
        //return $votes;
        return view('admin.votes.trash', compact('votes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\sHttp\Response
     */
    public function create()
    {
        return view('admin.votes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'name' => 'required | min:3',
                'CNIC' => 'required',
                'city' => 'required | min:4',
                'district' => 'required'
            ]);

        $vote = Vote::create([
            'name' => $request->name,
            'CNIC' => $request->CNIC,
            'city' => $request->city,
            'district' => $request->district,
        ]);
        $vote->save();

        return redirect('admin/votes')->with('message', 'record has been successfully added' );

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Vote  $votesiyt
     * @return \Illuminate\Http\Response
     */
    public function show(Vote $vote)
    {
        return view('admin.votes.view', compact('vote'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vote  $votes
     * @return \Illuminate\Http\Response
     */
    public function edit(Vote $vote)
    {

       // $votes = Vote::where('id', $vote->id)->get();
       // return $votes;
            return view('admin.votes.edit')->with('vote' , $vote);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vote  $votes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vote $vote)
    {
        $vote->name = $request->input("name");
        $vote->CNIC = $request->input("CNIC");
        $vote->city  = $request->input("city");
        $vote->district = $request->input("district");
        $vote->save();
            return redirect('/admin/votes')->with('message', 'Record Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vote  $votes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vote $vote)
    {
        if($vote->forceDelete()){
            return redirect('admin.votes.index')-> with('message', 'Deleted Successfully');
        } else {
            return redirect('admin.votes.index')->with('message', 'Delete Error');
        }
    }

    public function remove(Vote $vote){

        if($vote->delete()){
            return redirect('/admin.votes.trash')->with('message', 'Deleted Successfully');
        } else {
            return redirect('/admin.votes.trash')->with('message', 'Delete Error');
        }


    }
    public function restore($id){
        $vote = Vote::withTrashed()->findorfail($id);
        if ($vote->restore()){
        return redirect('/admin/votes')->with('message', 'Recored Restored');
            } else {
            return back() -> with('message', 'Recored Restore Error');
        }
    }
}
