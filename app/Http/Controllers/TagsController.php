<?php

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use Illuminate\Http\Request;
use function Sodium\compare;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required | min:3',
            'description' => 'required'
        ]);

         $tag = Tag::create([
            'title' => $request->title,
            'description' => $request->description
        ]);
         $tag->save();
        return redirect('/admin/tags')->with('message', 'Tag added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)

    {
        $tags = Tag::all();
        return view('admin.tags.article', compact('tags'));
    }

    public function search(Request $request){
        $ids = $request->input('tag_id');
        $article = Article::whereIn('id',$ids)->get();
        return redirect('/admin/tags/show', compact('article'));
    }

    public function articleShow(){
        return view('admin.tags.show', compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Responseweb
     */
    public function edit(Tag $tag)
    {
          return view('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $tag->title = $request->input('title');
        $tag->description = $request->input('description');
        $tag->save();
        return redirect('/admin/tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        if ($tag->forceDelete()){
            return redirect('/admin/tags')->with('message', 'tag deleted');
    } else {
            return redirect('/admin/tags')->with('message', 'Deletion error');
    }

    }
}
