<?php

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::latest('published_at')->published()->get();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trash()
    {
        $articles = Article::onlyTrashed()->paginate(2);
        return view('admin.articles.trash', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('admin.articles.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
               'title' => 'required| min:3',
               'description' => 'required| min:5'
         ]);

        $article = Article::create([
            'title' => $request->title,
            'description' => $request->description,
            'published_at' => $request->published_at
        ]);

        $article->tag()->attach($request->tag_id);
        return redirect('/admin/articles')->with('message', 'Article Published');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $atricle
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
            
        return view('/admin/articles/view')->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('/admin/articles/edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article->title = $request->input('title');
        $article->description = $request->input('description');
        $article->published_at = $request->input('published_at');
        $article->save();
        return redirect('/admin/articles')->with('message', 'Article updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        if ($article->forceDelete()){
            return redirect('/admin.index')->with('message', 'Record permanently deleted');
        }
        else{
            return redirect('admin.index')->with('message', 'Record deletion error');
        }
    }
     public function remove(Article $article)
    {
    if ($article->delete()){
        return redirect('/admin/articles/trash')->with('message', 'Record Trashed');
        }
    else{
        return redirect('/admin/articles/trash')->with('message', 'Record  Error');
    }

    }

    public function restore($id){
        $article =  Article::withTrashed()->findOrFail($id);

        if ($article->restore()){

            return redirect('/admin/articles')->with('message', 'Record Restore');
        }
        else {

            return redirect('/admin/articles')->with('message', 'Record restore error');
        }
    }
}
