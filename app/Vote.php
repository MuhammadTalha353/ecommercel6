<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static create(array $array)
 * @method static where(string $string, $id)
 * @method static findorfail($id)
 */
class Vote extends Model
{
    use SoftDeletes;
    protected $guarded= [];
    protected $dates = ['deleted_at'];

}
