<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static pagination(int $int)
 * @method static paginate(int $int)
 * @method static latest(string $string)
 * @method static find($id)
 * @method static where(string $string, string $string1, string $string2)
 * @method static whereIn(string $string, string $string1, array $array)
 */
class Article extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at',  'published_at'];


    public function setPublishedAtAttribute($date){
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date)->toDateTimeString();
    }
    public function scopePublished($query){
        $query->where('published_at', '<=' , Carbon::now());
    }

    public function scopeunPublished($query){
        $query->where('published_at', '>' , Carbon::now());
    }
    public function tag(){
        return $this->belongsToMany(Tag::class);
    }

    public function getFromDateAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

}
