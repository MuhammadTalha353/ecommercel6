<?php

namespace App;

use App\Http\Controllers\CategoryController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static paginate(int $int)
 */
class Category extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function product(){

        return $this->belongsToMany(Product::class);
    }
    public function children(){
        return $this->belongsToMany(Category::class,'category_parent', 'category_id',
            'parent_id');
    }


}

