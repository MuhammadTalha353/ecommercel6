<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['delete_at'];

    public function article(){
        return $this->belongsToMany(Article::class);
    }
}
